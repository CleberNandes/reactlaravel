<?php

use Illuminate\Database\Seeder;

use App\Product;

class ProductsTableSeeder extends Seeder
{
    public function run()
    {
        //cria um produto ???????????
        $faker = \Faker\Factory::create();

        factory(App\Product::class, 50)
            ->create()
            ->each(function ($candidato) {
            $candidato->make();
        });
    }
}